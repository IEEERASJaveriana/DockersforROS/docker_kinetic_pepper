#!/bin/bash

docker build -f build/pepper.Dockerfile -t pepperimage .
docker run -it \
	--env="DISPLAY" \
    	--env="QT_X11_NO_MITSHM=1" \
    	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--network host \
	--mount src="./app",target="/home/pepper/app",type=bind \
	pepperimage:latest
