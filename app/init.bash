#!/bin/bash

sudo apt-get install -y --allow-unauthenticated ros-kinetic-pepper-.*

cd pepper_ws
catkin_make
source /opt/ros/kinetic/setup.bash
source ~/app/pepper_ws/devel/setup.bash
echo "export ROS_HOSTNAME=localhost" >> ~/.bashrc && \
echo "export ROS_MASTER_URI=http://localhost:11311" >> ~/.bashrc
