FROM osrf/ros:kinetic-desktop-full

RUN apt-get update && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 4B63CF8FDE49746E98FA01DDAD19BAB3CBF125EA && \
    echo "deb http://snapshots.ros.org/kinetic/final/ubuntu xenial main" > /etc/apt/sources.list.d/ros1-snapshots.list && \
    apt-get install -y --allow-unauthenticated ros-kinetic-driver-base ros-kinetic-move-base-msgs -y ros-kinetic-octomap && \
    apt-get install -y --allow-unauthenticated ros-kinetic-octomap-msgs ros-kinetic-humanoid-msgs -y ros-kinetic-humanoid-nav-msgs && \
    apt-get install -y --allow-unauthenticated ros-kinetic-camera-info-manager -y ros-kinetic-camera-info-manager-py && \
    apt-get install -y --allow-unauthenticated ros-kinetic-moveit && \
    apt-get install -y nano && \
    apt-get install -y tmux && \
    apt-get install -y net-tools

RUN useradd -m -s /bin/bash -N -u 1000 pepper && \
    echo "pepper ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers && \
    chmod 0440 /etc/sudoers && \
    chmod g+w /etc/passwd 

USER pepper

WORKDIR home/pepper/app

COPY --chown=pepper app/ .


RUN chmod 774 ~/app/pepper_ws/

RUN /bin/bash -c "source /opt/ros/kinetic/setup.bash;"

ENV PYTHONPATH /home/pepper/app/naoqi/pynaoqi-python2.7-2.8.6.23-linux64/lib/python2.7/site-packages:$PYTHONPATH
