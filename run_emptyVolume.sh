#!/bin/bash

docker build -f build/pepper.Dockerfile -t pepperimage .
docker volume rm pepperVol
docker run --rm -it \
	--env="DISPLAY" \
    	--env="QT_X11_NO_MITSHM=1" \
    	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--network host \
	-v pepperVol:/app \
	pepperimage:latest
