# Pepper Control Application

## Table of contents
* [Description](#description)
* [Technologies](#technologies)
* [Setup](#setup)
* [Enable Pepper](#enable-pepper)
* [Licence](#licence)

## Description
This proyect was made for the ieee chapter "RAS Javeriana", it is a docker with all the necesary packages to control pepper using naoqi, load pepper in rviz and in gazebo. 

## Technologies
Project created with:
* Ubuntu: 16.04
* Docker: 23.0.6

## Setup
To run this project in ubuntu 20.04, you need to install these dependencies and run this commands to use docker:
```
$ sudo apt update
$ sudo apt install apt-transport-https ca-certificates curl software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
$ sudo apt install docker-ce
$ sudo usermod -aG docker ${USER}
```

If this is your first time running this docker, you need to run this commands:
```
$ git clone https://gitlab.com/IEEERASJaveriana/DockersforROS/docker_kinetic_pepper.git
$ cd docker_kinetic_pepper
$ ./run_emptyVolume.sh
$ source init.bash  #If you are asked to accept a licence write "yes"
```

If you have already runned this docker before use this commands:
```
$ cd docker_kinetic_pepper
$ ./run.sh
$ source init.bash  #If you are asked to accept a licence write "yes"
```

## Enable Pepper
These are the commands to enable all pepper topics:

tho know which is your network interface use this command:
```
$ sudo apt-get install net-tools
$ ifconfig
```

Mandatory!!!   

pepper Bringup: 
```
$ roslaunch pepper_dcm_bringup pepper_bringup.launch robot_ip:=[your robot ip] network_interface:=[your network interface]
```

Naoqi driver:
```
$ roslaunch naoqi_driver naoqi_driver.launch nao_ip:=[your network ip] network_interface:=[your network interface]
```

Microphone:
```
$ roslaunch naoqi_sensors_py microphone.launch nao_ip:=[your robot ip]
```

Pose:
```
$ roslaunch naoqi_pose pose_manager.launch nao_ip:=[your robot ip]
```

Sensors:
```
$ roslaunch pepper_sensors_py camera.launch nao_ip:=[your robot ip]
```

## Licence
ros_kinetic_pepper is available under the BSD-3-Clause license. See the LICENSE file for more details.